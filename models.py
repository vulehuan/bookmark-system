from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
import time

# Create your models here.
class TermData(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    weight = models.IntegerField(default = 0)
    parent = models.ForeignKey('self', null=True, blank=True)
    created_at = models.IntegerField()
    updated_at = models.IntegerField(null=True)
    def __unicode__(self):
        return self.name
    def clean(self):
        # prevent self referential when update
        if self.id is not None and self.parent.id == self.id:
            raise ValidationError("No self referential models")
    def save(self):
        # log timestamp when insert or update
        if self.created_at == None:
            self.created_at = time.time()
        else:
            self.updated_at = time.time()
        super(TermData, self).save()
        
    class Meta:
        db_table = 'bookmarks_term_data'

class Link(models.Model):
    user = models.ForeignKey(User)
    term_data = models.ManyToManyField(TermData)
    url = models.CharField(max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.IntegerField()
    updated_at = models.IntegerField(null=True)
    def __unicode__(self):
        return self.url
    def save(self):
        # log timestamp when insert or update
        if self.created_at == None:
            self.created_at = time.time()
        else:
            self.updated_at = time.time()
        super(Link, self).save()