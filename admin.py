from bookmarks.models import TermData, Link
from django.contrib import admin
import datetime

# format insert-log datetime
def ap_created_at(obj):
    return datetime.datetime.fromtimestamp(obj.created_at).strftime('%Y-%m-%d %H:%M')
ap_created_at.short_description = 'Created at'

class TermDataAdmin(admin.ModelAdmin):
    fields = ['parent', 'name', 'description', 'weight']
    list_display = ('name', ap_created_at)

class LinkAdmin(admin.ModelAdmin):
    fields = ['url', 'description', 'term_data']
    list_display = ('url', ap_created_at)
    search_fields = ['url']
    
    def save_model(self, request, obj, form, change):
        user = None
        try:
            user = getattr(obj, 'user', None)
        except Exception:
            pass
        if user is None:
            obj.user = request.user
        obj.save()
    
admin.site.register(TermData, TermDataAdmin)
admin.site.register(Link, LinkAdmin)
